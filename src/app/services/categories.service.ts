import { Injectable }  from '@angular/core';
import {Headers, Http,  Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import {Category} from './category';
@Injectable()
export class CategoriesService {

  private headers: Headers;
  private categoriesUrl = "http://ceban-api.azurewebsites.net/api/category/all";
  constructor(private http: Http) { 
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');

  }

  getCategory(): Observable<Category[]> {
    return this.http.get(this.categoriesUrl).map(res=> this.extractData(res)).catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    console.log(body.data)
    return body.data;
  }
  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.log(errMsg);
    return Observable.throw(errMsg);
  }

}
