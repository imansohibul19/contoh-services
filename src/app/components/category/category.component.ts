import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../services/categories.service';
import { Category } from '../../services/category';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  private category: Category[];
  private errorMessage: string;
  constructor(private categoriesService: CategoriesService) { }

  ngOnInit() {
    console.log("ngOnInit")
    this.getCategory();
    console.log(this.category)
  }

  getCategory (): void {
    this.categoriesService.getCategory().subscribe(cat => this.category=cat, er => console.log(er));
  }

}
